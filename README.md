# README #

Vocabulary anagrams 'game' for primary school English language learners (ELL)

## Instructions:
---------------

* Download the repo and update the credentials-skel.ini file with a secret key.  Save as credentials.ini in the vocab folder

* Build the docker image using the Dockerfile found in the 'vocab' folder.

* Use docker to run the container.

* Go to 0.0.0.0:5000 and have a blast.


## Authors
---------------

### Original Version:
* M Young

### Docker Version:
* **R Durairajan** [proj3-jsa](https://bitbucket.org/UOCIS322/proj3-jsa/)

### Updates to use AJAX:

* **Cole Sabin** [E-Mail](mailto:csabin@uoregon.edu)

